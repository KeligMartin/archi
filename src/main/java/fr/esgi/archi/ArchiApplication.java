package fr.esgi.archi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class ArchiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArchiApplication.class, args);
	}

}
