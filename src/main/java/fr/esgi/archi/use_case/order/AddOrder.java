package fr.esgi.archi.use_case.order;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.order.OrderDao;
import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.use_case.user.GetUserById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddOrder {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private GetUserById getUserById;

    public Order execute(Order order){
        User user = getUserById.execute(order.getUserId());
        if(user == null){
            return null;
        }
        return orderDao.save(order);
    }
}
