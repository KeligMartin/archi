package fr.esgi.archi.use_case.order;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.order.OrderDao;
import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.infrastructure.service.mail.MailSender;
import fr.esgi.archi.use_case.choice.AddChoice;
import fr.esgi.archi.use_case.user.GetUserById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ValidateOrder {

    @Autowired
    OrderDao orderDao;

    @Autowired
    AddChoice addChoice;

    @Autowired
    GetOrderById getOrderById;

    @Autowired
    GetUserById getUserById;

    @Autowired
    MailSender mailSender;

    public Order execute(int id){
        Order order = getOrderById.execute(id);
        User user = getUserById.execute(order.getUserId());
        mailSender.sendMail(user.getEmail(), "Choix validé pour la commande " + order.getId(), order.toString());
        return orderDao.validate(id);
    }
}
