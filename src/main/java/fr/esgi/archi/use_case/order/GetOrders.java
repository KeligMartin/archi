package fr.esgi.archi.use_case.order;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.order.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetOrders {

    @Autowired
    private OrderDao orderDao;

    public List<Order> execute(){
        return orderDao.findAll();
    }
}
