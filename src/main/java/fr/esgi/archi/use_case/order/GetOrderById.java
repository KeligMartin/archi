package fr.esgi.archi.use_case.order;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.order.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetOrderById {

    @Autowired
    private OrderDao orderDao;

    public Order execute(int id){
        return orderDao.findById(id);
    }
}
