package fr.esgi.archi.use_case.order;

import fr.esgi.archi.domain.choice.Choice;
import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.order.OrderDao;
import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.infrastructure.service.mail.MailSender;
import fr.esgi.archi.use_case.choice.AddChoice;
import fr.esgi.archi.use_case.user.GetUserById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AnswerOrder {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private AddChoice addChoice;

    @Autowired
    private GetUserById getUserById;

    @Autowired
    private MailSender mailSender;

    public Order answerOrder(Order order, String answer){
        User user = getUserById.execute(order.getUserId());
        addChoice.execute(new Choice(order.getUserId(), order.getId(), answer, new Date(System.currentTimeMillis())));
        mailSender.sendMail(user.getEmail(), "choix modifié pour la commande " + order.getId(), order.toString());
        return orderDao.answer(order, answer);
    }
}
