package fr.esgi.archi.use_case.order;

import fr.esgi.archi.domain.order.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteOrder {

    @Autowired
    private OrderDao orderDao;

    public void execute(int id){
        orderDao.deleteById(id);
    }
}
