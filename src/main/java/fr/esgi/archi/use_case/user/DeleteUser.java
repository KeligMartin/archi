package fr.esgi.archi.use_case.user;

import fr.esgi.archi.domain.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteUser {

    @Autowired
    private UserDao userDao;

    public void execute(int id){

        userDao.deleteById(id);
    }
}
