package fr.esgi.archi.use_case.user;

import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.domain.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetUsers {

    @Autowired
    private UserDao userDao;

    public List<User> execute(){
        return userDao.findAll();
    }
}
