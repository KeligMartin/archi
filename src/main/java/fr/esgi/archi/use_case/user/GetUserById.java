package fr.esgi.archi.use_case.user;

import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.domain.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetUserById {

    @Autowired
    private UserDao userDao;

    public User execute(int id){
        return userDao.findById(id);
    }
}
