package fr.esgi.archi.use_case.choice;

import fr.esgi.archi.domain.choice.ChoiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteChoices {
    @Autowired
    private ChoiceDao choiceDao;

    public void execute(){
        choiceDao.deleteAll();
    }
}
