package fr.esgi.archi.use_case.choice;

import fr.esgi.archi.domain.choice.Choice;
import fr.esgi.archi.domain.choice.ChoiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetChoices {
    @Autowired
    private ChoiceDao choiceDao;

    public List<Choice> execute(){
        return choiceDao.findAll();
    }
}
