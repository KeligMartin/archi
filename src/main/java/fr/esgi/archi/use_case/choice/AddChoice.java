package fr.esgi.archi.use_case.choice;

import fr.esgi.archi.domain.choice.Choice;
import fr.esgi.archi.domain.choice.ChoiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddChoice {

    @Autowired
    private ChoiceDao choiceDao;

    public void execute(Choice choice){
        choiceDao.save(choice);
    }
}
