package fr.esgi.archi.infrastructure.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.httpBasic().and().authorizeRequests().antMatchers(HttpMethod.GET, "/**").permitAll()
                .and().authorizeRequests().antMatchers(HttpMethod.POST, "/**").permitAll();
        httpSecurity.cors().and().csrf().disable();
    }
}
