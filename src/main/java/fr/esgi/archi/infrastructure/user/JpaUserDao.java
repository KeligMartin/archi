package fr.esgi.archi.infrastructure.user;

import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.domain.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class JpaUserDao implements UserDao {

     @Autowired
     private JpaUserRepository repository;

    public List<User> findAll(){
        return repository.findAll().stream()
                .map(jpaUser -> new User(jpaUser.getId(), jpaUser.getFirstName(), jpaUser.getLastName(), jpaUser.getEmail()))
                .collect(Collectors.toList());
    }

    public User findById(int id){
        Optional<JpaUser> jpaUser = repository.findById(id);
        return jpaUser.map(user -> new User(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail())).orElse(null);
    }

    public User save(User user){
        JpaUser jpaUser = new JpaUser(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
        repository.save(jpaUser);
        return user;
    }

    public void deleteById(int id){
        repository.deleteById(id);
    }
}
