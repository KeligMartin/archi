package fr.esgi.archi.infrastructure.user;

import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.domain.user.UserDao;
import fr.esgi.archi.infrastructure.service.SerializerUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

// @Repository
public class JacksonUserDao { // implements UserDao {

    // @Autowired
    SerializerUser serializerUser;


    public List<User> findAll(){
        return serializerUser.jsonToList();
    }

    public User findById(int id){
        return serializerUser.jsonToList()
                .stream()
                .filter(user -> user.getId() == id)
                .findAny()
                .orElse(null);
    }

    public User save(User user){
        return null;
    }

    public void deleteById(int id){}
}
