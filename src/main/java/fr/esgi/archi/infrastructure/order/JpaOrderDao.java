package fr.esgi.archi.infrastructure.order;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.order.OrderDao;
import fr.esgi.archi.use_case.choice.AddChoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class JpaOrderDao implements OrderDao {

    @Autowired
    private JpaOrderRepository repository;

    public List<Order> findAll(){
        return repository.findAll().stream()
                .map(jpaOrder -> new Order(jpaOrder.getId(), jpaOrder.getPrice(), jpaOrder.getUserId(), jpaOrder.getAnswer(), jpaOrder.getValid()))
                .collect(Collectors.toList());
    }

    public Order findById(int id){
        Optional<JpaOrder> jpaOrder = repository.findById(id);
        return jpaOrder.map(order -> new Order(order.getId(), order.getPrice(), order.getUserId(), order.getAnswer(), order.getValid())).orElse(null);
    }

    public Order save(Order order){
        JpaOrder jpaOrder = new JpaOrder(order.getId(), order.getPrice(), order.getUserId(), order.getAnswer(), order.getValid());
        repository.save(jpaOrder);
        return  order;
    }

    public void deleteById(int id){
        repository.deleteById(id);
    }

    public Order answer(Order order, String answer){
        JpaOrder jpaOrder = new JpaOrder(order.getId(), order.getPrice(), order.getUserId(), order.getAnswer(), order.getValid());
        repository.save(jpaOrder);
        return order;
    }

    public List<Order> findAllByUserId(int id){
        return repository.findAll().stream()
                .map(jpaOrder -> new Order(jpaOrder.getId(), jpaOrder.getPrice(), jpaOrder.getUserId(), jpaOrder.getAnswer(), jpaOrder.getValid()))
                .filter(jpaOder -> jpaOder.getUserId() == id)
                .collect(Collectors.toList());
    }

    public Order validate(int id){
        Optional<JpaOrder> jpaOrder = repository.findById(id);
        if(jpaOrder.isPresent()){
            jpaOrder.get().setValid(true);
            repository.save(jpaOrder.get());
        }
        return jpaOrder.map(order -> new Order(order.getId(), order.getPrice(), order.getUserId(), order.getAnswer(), order.getValid())).orElse(null);
    }
}
