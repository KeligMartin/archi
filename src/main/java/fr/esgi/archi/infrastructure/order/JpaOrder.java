package fr.esgi.archi.infrastructure.order;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class JpaOrder{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private double price;
    private int userId;
    private String answer;
    private boolean valid;

    public JpaOrder(){}

    public JpaOrder(int id, double price, int userId, String answer, boolean valid){
        this.id = id;
        this.price = price;
        this.userId = userId;
        this.answer = answer;
        this.valid = valid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean getValid(){
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @JsonIgnore
    public boolean isValid(){
        return this.price >= 0 && this.userId > 0;
    }
}
