package fr.esgi.archi.infrastructure.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface JpaOrderRepository extends JpaRepository<JpaOrder, Integer> {
}
