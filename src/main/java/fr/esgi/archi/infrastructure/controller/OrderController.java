package fr.esgi.archi.infrastructure.controller;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.use_case.order.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class OrderController {

    @Autowired
    private GetOrders getOrders;

    @Autowired
    private GetOrderById getOrderById;

    @Autowired
    private GetOrdersByUser getOrdersByUser;

    @Autowired
    private AddOrder addOrder;

    @Autowired
    private DeleteOrder deleteOrder;

    @Autowired
    private AnswerOrder answerOrder;

    @Autowired
    private ValidateOrder validateOrder;

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAll(){
        return new ResponseEntity<>(getOrders.execute(), HttpStatus.OK);
    }

    @GetMapping("/orders/users/{id}")
    public ResponseEntity<List<Order>> getAllByUserId(@PathVariable int id){
        return new ResponseEntity<>(getOrdersByUser.execute(id), HttpStatus.OK);
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getById(@PathVariable int id){
        Order order = getOrderById.execute(id);
        if(order == null || !order.isValid()){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("/orders/{id}/validate")
    public ResponseEntity<Order> validate(@PathVariable int id){
        Order updatedOrder = getOrderById.execute(id);
        if(updatedOrder == null || !updatedOrder.isValid()){
            return ResponseEntity.notFound().build();
        }
        if(updatedOrder.getAnswer().equals("undefined")){
            return ResponseEntity.badRequest().build();
        }
        Order validatedOrder = validateOrder.execute(id);
        return new ResponseEntity<>(validatedOrder, HttpStatus.OK);
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Order> answerOrder(@PathVariable int id, @RequestBody Order order){
        Order updateOrder = getOrderById.execute(id);
        if(updateOrder == null || !updateOrder.isValid()){
            return ResponseEntity.notFound().build();
        }
        if(!order.getAnswer().equals("reimburse") && !order.getAnswer().equals("keep")){
            return ResponseEntity.badRequest().build();
        }
        updateOrder.setAnswer(order.getAnswer());
        answerOrder.answerOrder(updateOrder, order.getAnswer());
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PostMapping("/orders")
    public ResponseEntity<Order> addOrder(@RequestBody Order order){
        Order addedOrder = addOrder.execute(order);
        if(addedOrder == null || !addedOrder.isValid()){
            return ResponseEntity.badRequest().build();
        }
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Order> deleteOrder(@PathVariable int id){
        Order order = getOrderById.execute(id);
        if(order == null){
            return ResponseEntity.notFound().build();
        }
        deleteOrder.execute(id);
        return new ResponseEntity<>(order, HttpStatus.NO_CONTENT);
    }
}
