package fr.esgi.archi.infrastructure.controller;

import fr.esgi.archi.domain.user.User;
import fr.esgi.archi.use_case.user.AddUser;
import fr.esgi.archi.use_case.user.DeleteUser;
import fr.esgi.archi.use_case.user.GetUserById;
import fr.esgi.archi.use_case.user.GetUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private GetUsers getUsers;

    @Autowired
    private AddUser addUser;

    @Autowired
    private GetUserById getUserById;

    @Autowired DeleteUser deleteUser;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAll(){
        return new ResponseEntity<>(getUsers.execute(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getById(@PathVariable int id){
        User user = getUserById.execute(id);
        if(user == null){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<User> addUser(@RequestBody User user){
        User addedUser = addUser.execute(user);

        if(addedUser == null){
            return ResponseEntity.badRequest().build();
        }

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable int id) {
        User user = getUserById.execute(id);
        if( user == null){
            return ResponseEntity.notFound().build();
        }
        deleteUser.execute(id);
        return new ResponseEntity<>(user, HttpStatus.NO_CONTENT);
    }
}