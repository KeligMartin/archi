package fr.esgi.archi.infrastructure.controller;

import fr.esgi.archi.domain.choice.Choice;
import fr.esgi.archi.use_case.choice.DeleteChoices;
import fr.esgi.archi.use_case.choice.GetChoices;
import fr.esgi.archi.use_case.choice.GetChoicesByUserId;
import fr.esgi.archi.use_case.choice.GetLastChoices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChoiceController {

    @Autowired
    private GetChoices getChoices;

    @Autowired
    private GetChoicesByUserId getChoicesByUserId;

    @Autowired
    private DeleteChoices deleteChoices;

    @Autowired
    private GetLastChoices getLastChoices;

    @GetMapping("/choices")
    public ResponseEntity<List<Choice>> getAll(){
        return new ResponseEntity<>(getChoices.execute(), HttpStatus.OK);
    }

    @GetMapping("/last-choices")
    public ResponseEntity<List<Choice>> getLasts(){
        return new ResponseEntity<>(getLastChoices.execute(), HttpStatus.OK);
    }

    @GetMapping("/choices/users/{id}")
    public ResponseEntity<List<Choice>> getByUserId(@PathVariable int id){
        return new ResponseEntity<>(getChoicesByUserId.execute(id), HttpStatus.OK);
    }

    @DeleteMapping("/choices")
    public ResponseEntity<Choice> deleteAll(){
        deleteChoices.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
