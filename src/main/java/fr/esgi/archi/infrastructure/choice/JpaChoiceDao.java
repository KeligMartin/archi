package fr.esgi.archi.infrastructure.choice;

import fr.esgi.archi.domain.choice.Choice;
import fr.esgi.archi.domain.choice.ChoiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class JpaChoiceDao implements ChoiceDao {

    @Autowired
    private JpaChoiceRepository repository;

    @Override
    public List<Choice> findAll() {
        return repository.findAll().stream()
                .map(jpaChoice -> new Choice(jpaChoice.getId(), jpaChoice.getUserId(), jpaChoice.getOrderId(), jpaChoice.getAnswer(), jpaChoice.getTime()))
                .collect(Collectors.toList());
    }

    @Override
    public Choice findById(int id) {
        Optional<JpaChoice> jpaChoice = repository.findById(id);
        return jpaChoice.map(choice -> new Choice(choice.getId(), choice.getUserId(), choice.getOrderId(), choice.getAnswer(), choice.getTime())).orElse(null);
    }

    @Override
    public List<Choice> findByUserId(int userId) {
        return repository.findAll().stream()
                .map(jpaChoice -> new Choice(jpaChoice.getId(), jpaChoice.getUserId(), jpaChoice.getOrderId(), jpaChoice.getAnswer(), jpaChoice.getTime()))
                .filter(jpaOrder -> jpaOrder.getUserId() == userId)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public Choice save(Choice choice) {
        JpaChoice jpaChoice = new JpaChoice(choice.getId(), choice.getUserId(), choice.getOrderId(), choice.getAnswer(), choice.getTime());
        repository.save(jpaChoice);
        return choice;
    }

    @Override
    public List<Choice> findLastChoices(){
        Comparator<JpaChoice> descTime = (j1, j2) -> j2.getTime().compareTo(j1.getTime());

        List<Choice> choices = repository.findAll().stream()
                .sorted(descTime)
                .map(jpaChoice -> new Choice(jpaChoice.getId(), jpaChoice.getUserId(), jpaChoice.getOrderId(), jpaChoice.getAnswer(), jpaChoice.getTime()))
                .collect(Collectors.toList());

        Set<Integer> set = new HashSet<>(choices.size());
        choices.removeIf(choice -> !set.add(choice.getUserId()));

        return choices;
    }
}
