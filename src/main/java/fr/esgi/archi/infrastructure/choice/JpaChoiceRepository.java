package fr.esgi.archi.infrastructure.choice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaChoiceRepository extends JpaRepository<JpaChoice, Integer>{}
