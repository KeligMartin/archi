package fr.esgi.archi.infrastructure.choice;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class JpaChoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int userId;
    private int orderId;
    private String answer;
    private Date time = new Date(System.currentTimeMillis());

    public JpaChoice() {}

    public JpaChoice(int id, int userId, int orderId, String answer, Date time) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
        this.answer = answer;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @JsonIgnore
    public boolean isValid(){
        return this.userId > 0 && !this.answer.equals("undefined");
    }
}
