package fr.esgi.archi.infrastructure.service;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import fr.esgi.archi.domain.order.Order;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Service
public class SerializerOrder {
    ObjectMapper mapper;
    ObjectWriter writer;
    File filePath;

    public SerializerOrder(){
        filePath = Paths.get("src/main/resources/data/orders.json").toFile();

        mapper = new ObjectMapper();
        writer = mapper.writer(new DefaultPrettyPrinter());
    }

    public List<Order> jsonToList() throws IOException {
        Order[] ordersArray = mapper.readValue(filePath, Order[].class);
        return Arrays.asList(ordersArray);
    }

    public void listToJson(List<Order> orders) throws IOException {
        writer.writeValue(filePath, orders);
    }
}
