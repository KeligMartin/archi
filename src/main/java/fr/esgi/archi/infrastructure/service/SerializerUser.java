package fr.esgi.archi.infrastructure.service;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import fr.esgi.archi.domain.user.User;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Service
public class SerializerUser {

    ObjectMapper mapper;
    ObjectWriter writer;
    File filePath;

    public SerializerUser(){
        filePath = Paths.get("src/main/resources/data/users.json").toFile();

        mapper = new ObjectMapper();
        writer = mapper.writer(new DefaultPrettyPrinter());
    }

    public List<User> jsonToList() {
        try{
            User[] usersArray = mapper.readValue(filePath, User[].class);
            return Arrays.asList(usersArray);
        } catch(IOException ioException){
            ioException.printStackTrace();
        }
        return null;
    }

    public void listToJson(List<User> users) throws IOException {
        writer.writeValue(filePath, users);
    }

}
