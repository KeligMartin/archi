package fr.esgi.archi.infrastructure.service.mail;

public interface MailSender {

    void sendMail(String recipient, String title, String content);
}
