package fr.esgi.archi.infrastructure.service.mail;

import org.springframework.stereotype.Service;

public class MockMailService implements MailSender {

    @Override
    public void sendMail(String recipient, String title, String content) {
        System.out.println("Mail envoyé à " + recipient + " -> choix validé pour la commande " + title + " réponse : " + content);
    }
}
