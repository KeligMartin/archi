package fr.esgi.archi.domain.choice;

import java.util.List;

public interface ChoiceDao {

    List<Choice> findAll();

    Choice findById(int id);

    List<Choice> findByUserId(int userId);

    List<Choice> findLastChoices();

    void deleteAll();

    Choice save(Choice choice);
}
