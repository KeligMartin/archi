package fr.esgi.archi.domain.choice;

import java.util.Date;

public class Choice {
    private int id;
    private int userId;
    private  int orderId;
    private String answer;
    private Date time = new Date(System.currentTimeMillis());

    public Choice() {}

    public Choice(int id, int userId, int orderId, String answer, Date time) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
        this.answer = answer;
        this.time = time;
    }

    public Choice(int userId, int orderId, String answer, Date time) {
        this.userId = userId;
        this.orderId = orderId;
        this.answer = answer;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
