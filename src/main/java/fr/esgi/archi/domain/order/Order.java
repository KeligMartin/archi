package fr.esgi.archi.domain.order;

public class Order{

    private int id;
    private double price;
    private int userId;
    private String answer = "undefined";
    private boolean valid;

    public Order(){}

    public Order(int id, double price, int userId, String answer, boolean valid){
        this.id = id;
        this.price = price;
        this.userId = userId;
        this.answer = answer;
        this.valid = valid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean getValid(){
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isValid(){
        return id >= 0 && price >= 0
                && userId > 0
                && !answer.equals("");
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", userId=" + userId +
                ", answer='" + answer + '\'' +
                ", valid=" + valid +
                '}';
    }
}
