package fr.esgi.archi.domain.order;

import java.util.List;

public interface OrderDao {

    List<Order> findAll();

    Order findById(int id);

    List<Order> findAllByUserId(int id);

    Order save(Order order);

    void deleteById(int id);

    Order answer(Order order, String answer);

    Order validate(int id);
}
