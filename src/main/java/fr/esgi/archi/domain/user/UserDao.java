package fr.esgi.archi.domain.user;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    User findById(int id);

    User save(User user);

    void deleteById(int id);
}
