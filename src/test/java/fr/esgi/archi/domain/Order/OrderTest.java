package fr.esgi.archi.domain.Order;

import fr.esgi.archi.domain.order.Order;
import fr.esgi.archi.domain.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderTest {

    @Mock
    Order order;

    @Mock
    User user;

    @BeforeEach
    public void beforeTest() {
        this.user = new User(1, "firstName", "lastName", "email@email.fr");
        this.order = new Order(0, 100, this.user.getId(), "answer", true);
    }

    @Test
    public void testIsValidNominal(){
        assertTrue(this.order.isValid());
    }

    @Test
    public void testIsNotValidPrice(){
        this.order.setPrice(-1);
        assertFalse(this.order.isValid());
    }

    @Test
    public void testIsNotValidUserId(){
        this.order.setUserId(-1);
        assertFalse(this.order.isValid());
    }

    @Test
    public void testIsNotValidAnswer(){
        this.order.setAnswer("");
        assertFalse(this.order.isValid());
    }
}
