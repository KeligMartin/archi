package fr.esgi.archi.domain.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    @Mock
    User user;

    @BeforeEach
    public void beforeTest() {
        this.user = new User(1, "firstName", "lastName", "email@email.fr");
    }

    @Test
    public void testIsValidNominal(){
        assertTrue(this.user.isValid());
    }

    @Test
    public void testIsNotValidFirstName(){
        this.user.setFirstName(null);
        assertFalse(this.user.isValid());
    }

    @Test
    public void testIsNotValidLastName(){
        this.user.setLastName(null);
        assertFalse(this.user.isValid());
    }

    @Test
    public void testIsNotValidEmailFormat(){
        this.user.setEmail(null);
        assertFalse(this.user.isValid());
    }

    @Test
    public void testIsNotValidId(){
        this.user.setId(-1);
        assertFalse(this.user.isValid());
    }
}
